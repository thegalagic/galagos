<!--
SPDX-FileCopyrightText: 2020 Community Rule, Media Enterprise Design Lab <https://colorado.edu/lab/medlab>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Benevolent Dictator

One community member, the benevolent dictator (BD), holds ultimate
decision-making power, aided by a Board. The Board will eventually become the
basis of a more inclusive governance structure.

* **Autocracy** The benevolent dictator (BD) holds ultimate decision-making
  power in the community.
  * **Executive** The BD is responsible for implementing—or delegating
    implementation of—policies and other decisions.
  * **Lobbying** If participants are not happy with the BD's leadership, they
    may voice their concerns or leave the community.
* **Board** A board advises the BD.
  * **Membership** The BD invites active, committed participants to join the
    Board, whose members help the BD in managing the community.
  * **Evolution** When the Board reaches 5 members, including the BD, the
    Board assumes control of the community. This control is activated upon the
    Board unanimously adopting a new Rule. Until then, the BD can change the
    governance structure of the community at will.
* **Do-ocracy** Those who step forward to do a given task can decide how it
  should be done, within the guidelines that the BD sets.
  * **Membership** Participation is open to anyone who wants to join. The BD
    can remove misbehaving participants at will for the sake of the common
    good.

## Participants

Participation is open to anyone who wants to join.

The BD can remove misbehaving participants at will for the sake of the common good.

The BD invites active, committed participants to join the Board, whose members
help the BD in managing the community.

In the event that the BD is unable or unwilling to continue leadership, the BD
may appoint a new BD or choose to alter the governance structure entirely.

## Policy

The BD sets the community's policies and makes decisions for the community,
taking reasonable account of input from other participants.

The BD is responsible for implementing—or delegating implementation of—policies
and other decisions.

If participants are not happy with the BD's leadership, they may voice their
concerns or leave the community.

## Process

Community participants are free to discuss and debate community policies,
practices, and culture.

## Evolution

When the Board reaches 5 members, including the BD, the Board assumes control of
the community. This control is activated upon the Board unanimously adopting a
new Rule. Until then, the BD can change the governance structure of the
community at will.

---

[![CommunityRule derived](https://communityrule.info/assets/CommunityRule-derived-000000.svg)](https://communityrule.info)

[Creative Commons BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)
