<!--
SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>
SPDX-FileCopyrightText: 2020-4 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

galagos generates new FLOSS repositories and keeps them up to date.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/galagos/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Galagos

This repository acts as a template for new FLOSS git repositories that meet
[REUSE compliance](https://reuse.software/) for licensing. Additionally
it ensures those repositories can continue to get updates from this template.

Note that this template is in ALPHA status and could cause lost work.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
  - [New Repositories](#new-repositories)
  - [Migration](#migration)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)

<!-- vim-markdown-toc -->

## Background

Conventions have emerged for FLOSS repositories. Following these conventions
requires a bit of work to put files in the right places with the right content.
This is an attempt to automate that work. Specifically it aims to leave you with
a repository that:

- contains skeletons of the standard files e.g. `README`, `AUTHORS`, `CHANGELOG`,
  `CODE_OF_CONDUCT`, `CONTRIBUTING`
- is demonstrably compliant with the [REUSE
  Specification](https://reuse.software/spec/). A suggested build file (ninja)
  is supplied that will apply license and copyright headers and test for
  compliance.
- provides a means to stay up-to-date with this repository. The build file
  includes an update target that will pull from here.
- allows you to relicense as many of the skeleton files as possible to your
  license(s) of choice.

## Install

[Copier](https://copier.readthedocs.io/en/latest/) is a prerequisite, see their repo
for installation instructions.

OPTIONAL: to apply license and copyright headers with our build targets via the
suggested ninja build file then [ninja](ninja-build.org/) and
[reuse-tool](https://github.com/fsfe/reuse-tool) are also required.

## Usage

### New Repositories

Create or change to the dir for your new repository. Then run:

```bash
git init
# Choose one:
copier copy git@gitlab.com:thegalagic/galagos .       # SSH
copier copy https://gitlab.com/thegalagic/galagos .   # HTTPS
```

You'll be prompted for some information:

- Project name: the short name of the project. Should match the repository's
  directory name.
- Project description: a short description of the project. This will be used in
  licensing headers to give context, no more than a sentence or two is needed,
  it should terminate with a full stop. This is a multiline field so you can use
  hard wrapping on long descriptions to avoid annoying linters that check line
  length in files.
- Project repository URL: the intended URL of the repository. This is used in
  licensing headers to refer people to the canonical location of information if
  lacking from their copy. It does not have to exist.
- Copyright statement: the people or organisation who retain the copyright.
  This will be included in all copyright headers.
- Copyright year(s): the copyright year(s) to apply to copyright
  statements in licensing headers.

Once completed you'll have a new repo with a bunch of skeleton documentation and
a ninja build. The build offers:

- An empty `build` target
- A `test` target that will check with `reuse` for license compliance
- An `update` target that will update the repo from the latest available in this
  repo.
- An empty `deploy` target

Next steps:

- Apply copyright and licensing headers to files that lack them. Inspect
  `build.ninja` to see the defaults then run `ninja _addheaders` to apply.
  `ninja test` should then succeed, which relies on the
  [reuse-tool](https://github.com/fsfe/reuse-tool) to check for compliance with
  the REUSE Specification.
- Add additional documentation regarding your project.
  - Flesh out the `README`
    - Consider following the [Standard README](https://github.com/RichardLitt/standard-readme/)
    - In the long description consider adding a link to the official
      repository in case users find your doc through a mirror.
    - Add badges if you have them. Consider registering for a
      [CII Best Practices](https://bestpractices.coreinfrastructure.org/en) badge.
    - Choose a governance model and document in your own `GOVERNANCE.md`.
      [CommunityRule](https://communityrule.info/) is a good place to start.
  - In `CONTRIBUTING.md`
    - It's worth having a 'Releasing' section if you release and more detail on
      expected code quality in 'Code Changes'.
    - Review the License section also to ensure it aligns with your project.
    - Review the Vulnerability Reporting section in particular update the
      suggested email address to a valid one.

For on-going maintenance:

- To adjust your copyright and license header information such as project
  description, URL and copyright year(s) run `copier update`, never update the
  copier answers file manually: [updating a project -
  copier](https://copier.readthedocs.io/en/latest/updating/#never-change-the-answers-file-manually)
- Run `ninja update` to pull the latest changes from this repository. Copier
  will insist the repo has no outstanding changes first.

### Migration

These notes apply to existing repos that were using a prior version (\<=0.0.3) of
this template. You need to:

- Start from a clean repo `git status`.
- Consult previous answers in `galagos/local/settings.bara.sky` and convert to
  a new copier yaml file `.copier-answers.yml`. Bear in mind the bug around long
  project descriptions, you might need to force a carriage return (see bugs
  section below).

```yaml
copyright_statement: <consult existing license headers>
copyright_years: '2022' # if this is a single year the 'quote' it
project_desc: Project description
project_name: Project Name
repo_url: https://gitlab.com/repo
```

- Run the initialisation:
  `copier copy https://gitlab.com/thegalagic/galagos/template .`
  Accept all overwrites.
- Delete excess files and git add new copier answers file:

```bash
rm -rf galagos/local
rm galagos/copy.bara.sky
git add .copier-answers.yml .copier-answers.yml.license
```

- Add licensing headers to files (files that copier has overwritten will be
  lacking them):

```bash
ninja _addheaders
```

- Carefully examine `git diff`, there will definitely be too many deletions in
  some files as the skeleton versions have been added from the template. However
  there should be no changes to license headers in any file.

  - For `build.ninja` you can ignore all changes except the addition of an
    addheader task for `.copier-answers.yml` and you can remove those for
    `galagos/copy.bara.sky` and `galagos/local/settings.bara.sky`

- Ensure build and test works as before. Commit when happy.

## Roadmap

BETA roadmap:

- Copier supports recursive templates with the subdirectory parameter which we
  are already using. See if we can apply our own template to this repo.
  [Configuring a template - copier](https://copier.readthedocs.io/en/latest/configuring/#subdirectory)
- Flesh out README from README specification. Perhaps sections can be commented
  to allow choice.
- Add tests:
  - Initial layout is as expected for given answers
    - Single year is quoted '2023' whereas multiple is not '2022-3' check both
      are fine
  - Changing an answer like copyright year/statement works
    - Licenses updated by build correctly
    - A copier update/upgrade does not change anything
  - Test we can apply licensing ok to new files
  - Test we can update ok, nothing changes
  - Test first build succeeds
  - Can we test that galagos itself resembles a newly generated repo? There's a
    danger its own files (README, AUTHORS) get out of step with the repos it
    bootstraps.
- We have created our own .reuse templates for this repo in `.reuse/templates`.
  It's a shame to duplicate these. We could instead symlink to the ones in
  template/ but they have placeholders in them. If reuse supported more
  variables in its templates we could solve this.

Next Features:

- Test our README follows best-practice via some linter. See [Standard README](https://github.com/RichardLitt/standard-readme)
- Test our CHANGELOG in a similar way
- In CONTRIBUTING generate standard sections for what's expected from code in
  different languages
- Allow changes in a repository to be pushed back to this one

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue and PRs will be considered. For full details see
[CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

- All files that are copied to new repos are licensed under CC0 so that they may
  be used without any conditions - in particular so that they can be used without
  their license and copyright notices. This simplifies matters for new
  repositories who need only include their own license and copyright headers on
  those files.
  - CC0 was chosen as it is widely used, accepted and comprehensive (versus e.g.
    'The Unlicense'). It lacks a grant of patent rights - however the simplicity
    of the work makes that largely irrelevant we believe.
  - There are three exceptions to this:
    - `bootstrap/CONTRIBUTING.md` which must remain under a CC-BY-SA-3.0
      compatible license as it is derived from a work under that license.
    - `bootstrap/CODE_OF_CONDUCT.md` as it is copyright Coraline da Ehmke.
    - `galagos/local/settings.bara.sky` as new repos should create this
      themselves, it is not copied over to them. This file belongs to
      this repo and is licensed as code under GPL-3.0-or-later.
- All other source code is licensed under GPL-3.0-or-later.
- Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
- Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

galagos is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
