<!--
SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>
SPDX-FileCopyrightText: 2020-4 Galagic Limited, et al. <https://galagic.com>

SPDX-License-Identifier: CC-BY-SA-4.0

galagos generates new FLOSS repositories and keeps them up to date.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/thegalagic/galagos/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## \[Unreleased\]

## 2.0.0 - 2024-06-26

### Added

- New template for the `CC-BY-ND` license which I'd like to use for all viewpoint
  files in future like writing on websites, blogs etc. Non-derivative is important.

### Changed

- Updated to work with reuse v3.0.0+ specifically some commands have been renamed
  e.g. `addheader` is now `annotate` and `--explicit-license` is now gone.
- Update documentation to work with copier v9 which now need explicit `copy`
  command added instead of defaulting to that action.
- Tweaked phrasing of license section in `CONTRIBUTING.md`

### Removed

- No longer using `build_config.ninja`. Moving repos to use `devenv.sh` which
  allows us to use a `devenv.local.nix` file instead which has more flexibility
  and is also more standard.

## 1.0.2 - 2023-02-16

### Fixed

- CONTRIBUTING.MD has changed:
  - Formatting of the DCO was triggering mdl markdown linting in
    downstream projects due to too many leading spaces.
  - Removed hard-coded email address from CONTRIBUTING and draw attention to the
    security section in the README as it definately needs review on new
    projects.

## 1.0.1 - 2023-01-25

### Fixed

- Make project description multiline to allow use of newlines for hard wrapping.
  Long lines in files can trigger lint warnings etc.

## 1.0.0 - 2023-01-11

### Changed

- Breaking: use [copier](https://copier.readthedocs.io/en/latest/) rather than
  copybara as our template tool. Migration notes have been added to our README.

### Fixed

- You can now specify a custom copyright statement rather than it being
  hard-coded to Galagic.

## 0.0.3 - 2022-11-08

### Fixed

- It should be 'et al' not 'et. al' in our copyright statement (thanks to
  proselint).

### Added

- Table of contents to CONTRIBUTING.

## 0.0.2 - 2022-01-31

### Fixed

- The AGPL template was hard-coded for Figular, should be fully templated.

## 0.0.1 - 2021-01-22

### Added

- New template for AGPL-3.0-or-later.
- More detail in README regarding usage and licensing.
